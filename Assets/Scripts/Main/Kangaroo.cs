using UnityEngine;

// INHERITANCE
public class Kangaroo : Animal
{
    void Start()
    {
        this.AnimalName = "Kangaroo";
        this.JumpSize = 2.3f;
        this.JumpInterval = 3f;
        AnimalRb = GetComponent<Rigidbody>();
        StartCoroutine(base.Move());
    }

    //POLYMORPHISM
    public override Vector3 JumpDirection()
    {
        return new Vector3(0, JumpSize, JumpSize);
    }
}