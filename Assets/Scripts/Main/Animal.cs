using UnityEngine;
using System.Collections;

public abstract class Animal : MonoBehaviour
{
    private string _animalName;
    private string _animalType;
    private Rigidbody _animalRb;
    [SerializeField] private float _jumpSize;
    [SerializeField] private float _jumpInterval;

    // ENCAPSULATION
    protected string AnimalName {get => _animalName; set => _animalName = value; }
    protected string AnimalType {get => _animalType; set => _animalType = value; }
    protected float JumpSize { get => _jumpSize; set => _jumpSize = value; }
    protected float JumpInterval { get => _jumpInterval; set => _jumpInterval = value; }

    protected Rigidbody AnimalRb { get => _animalRb; set => _animalRb = value; }

    // ABSTRACTION
    public abstract Vector3 JumpDirection();

     protected void Jump()
    {
        this.AnimalRb.AddForce(JumpDirection() * JumpSize, ForceMode.Impulse);
    }

    protected void GetAnimalType()
    {
        Debug.Log("The animal is a " + _animalType);
    }

    protected IEnumerator Move()
    {
        while (true)
        {
            Jump();
            yield return new WaitForSeconds(_jumpInterval);
        }
    }

}