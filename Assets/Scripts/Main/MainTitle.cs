using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MainTitle : MonoBehaviour
{
    private TextMeshProUGUI title;
    // Start is called before the first frame update
    void Start()
    {
        title = GetComponent<TextMeshProUGUI>();
        title.text = "Hello <color=green>" + TitleManager.username + "</color>!<br>" + "The <color=blue>Rabbit</color> and the <color=red>Kangaroo</color> are both Animals";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
