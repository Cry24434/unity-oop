using UnityEngine;

// INHERITANCE
public class Rabbit : Animal
{
    void Start()
    {
        this.AnimalName = "Rabbit";
        this.JumpSize = 2f;
        this.JumpInterval = 0.9f;
        AnimalRb = GetComponent<Rigidbody>();
        StartCoroutine(base.Move());
    }

    //POLYMORPHISM
    public override Vector3 JumpDirection()
    {
        return new Vector3(0, JumpSize / 2, JumpSize);
    }
}